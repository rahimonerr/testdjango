from typing import List
from datetime import datetime


class Chart:
    height: int
    type: str

    def __init__(self, height: int, type: str) -> None:
        self.height = height
        self.type = type


class DataLabels:
    enabled: bool

    def __init__(self, enabled: bool) -> None:
        self.enabled = enabled


class Series:
    name: str
    data: List[int]

    def __init__(self, name: str, data: List[int]) -> None:
        self.name = name
        self.data = data


class Stroke:
    curve: str

    def __init__(self, curve: str) -> None:
        self.curve = curve


class X:
    format: str

    def __init__(self, format: str) -> None:
        self.format = format


class Tooltip:
    x: X

    def __init__(self, x: X) -> None:
        self.x = x


class Xaxis:
    type: str
    categories: List[datetime]

    def __init__(self, type: str, categories: List[datetime]) -> None:
        self.type = type
        self.categories = categories


class ChartData:
    chart: Chart
    data_labels: DataLabels
    stroke: Stroke
    colors: List[str]
    series: List[Series]
    xaxis: Xaxis
    tooltip: Tooltip

    def __init__(self, chart: Chart, data_labels: DataLabels, stroke: Stroke, colors: List[str], series: List[Series], xaxis: Xaxis, tooltip: Tooltip) -> None:
        self.chart = chart
        self.data_labels = data_labels
        self.stroke = stroke
        self.colors = colors
        self.series = series
        self.xaxis = xaxis
        self.tooltip = tooltip
