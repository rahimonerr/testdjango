# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

import json
from typing import Optional
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from json import dumps, loads
from django.http import JsonResponse
from app.chart import *
import pyodbc
import mysql.connector


@login_required(login_url="/login/")
def index(request):

    context = {}
    context['segment'] = 'index'

    #html_template = loader.get_template( 'index.html' )
    # return HttpResponse(html_template.render(context, request))
    return render(request, "index.html", context)


@login_required(login_url="/login/")
def pages(request):
    context = {}

    try:
        load_template = request.path.split('/')[-1]
        context['segment'] = load_template
        if load_template == "ajax.html":
            rows = connect_server_mysql()
            options = {}
            # options['chart1'] = get_step(rows)
            options["chart2_series"], options['chart2_categories'], options['chart3_series'] = get_daily_read(
                rows)
            options["chart4_series"], options["chart4_categories"] = send_data(
                rows)
            return JsonResponse(options)
        else:
            return render(request, load_template, context)

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:

        html_template = loader.get_template('page-500.html')
        return HttpResponse(html_template.render(context, request))


def validate_username(request):

    data = {
        'is_taken': "data"
    }
    return JsonResponse(data)


# def connect_server():
#     server = '35.158.157.146'
#     database = 'TIMDB_PROD'
#     username = 'test'
#     password = '1234'
#     try:
#         cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=' +
#                               server+';DATABASE='+database+';UID='+username+';PWD=' + password)

#     except Exception as ex:
#         cnxn = 0
#     cursor = cnxn.cursor()
#     cursor.execute(
#         "SELECT * FROM ["+database+"].[dbo].[LogDemo] WHERE Date BETWEEN '2021-07-12' AND '2021-07-13'")
#     rows = cursor.fetchall()
#     return rows


def connect_server_mysql():
    server = '167.99.135.64'
    database = 'iwo_lr_imu'
    table = 'tofas'
    username = 'pyUser'
    password = 'Py123456'
    try:
        cnxn = mysql.connector.connect(
            user=username, password=password, host=server, database=database)

    except Exception as ex:
        cnxn = 0
    cursor = cnxn.cursor()

    try:
        cursor.execute(
            "SELECT * FROM "+table+" WHERE time BETWEEN '2021-06-22' AND '2021-06-23'")
        rows = cursor.fetchall()
    except Exception as ex:
        cnxn = 0

    return rows


def get_step(rows):
    step_cnt = []
    cnt = []
    step = ''
    steps = []
    step = row[3]
    for row in rows:
        if (row[5] != 'tim.lora' and rows[5] != 'http://onelink.to/mbennm'):
            steps = row+1
            for j in range(7):
                if step[row] < step[row + 1]:
                    cnt[j] = step[row+1]-step[row]
                else:
                    cnt[j] = step[row]
                row += 1
            for a in range(7):
                step_cnt[row] = step_cnt[row] + cnt[a]
    return step_cnt


# def time_spent(rows):
#     time_axis = set()
#     step_cnt = []
#     for row in rows:
#         time_axis.add(row.time.strftime("%Y/%m/%d, %H:%M:%S"))
#     time_axis = list(time_axis)
#     time_axis.sort()
#     for row in rows:
#         if rows.barcode != 'tim.lora' and rows.barcode != 'http://onelink.to/mbennm':

#     return step_cnt

# def get_battery_level(rows):

#     array = []
#     series_dic = {}
#     cnt = 0
#     for row in rows:
#         if row.Message == "Device Info POST":
#             state = False
#             for i in range(len(array)):
#                 if array[i]["name"] == row.DeviceId:
#                     cnt = i
#                     state = True
#             if state != True:
#                 array.append({"name": row.DeviceId, "data": []})
#                 array[-1]["data"].append(
#                     [row.Date.strftime("%Y/%m/%d, %H:%M"), row.DeviceBatteryLevel])
#             else:
#                 array[cnt]["data"].append(
#                     [row.Date.strftime("%Y/%m/%d, %H:%M"), row.DeviceBatteryLevel])

#     for i in range(len(array)):
#         array[i]["data"] = array_down(array[i]["data"], 10)
#     return array


# def array_down(array, number):
#     length = len(array)
#     for i in range(length):
#         if(i % number != 0):
#             array.pop(length - i)
#     return array


# def get_daily_read(rows):
#     time_axis = set()
#     users = set()
#     devices = set()
#     address = set()
#     for row in rows:
#         if row.Message == "Device Info POST":
#             time_axis.add(row.Date.strftime("%Y/%m/%d"))
#             devices.add(row.DeviceId)
#             users.add(row.UserName)
#             address.add(row.Address)
#     time_axis = list(time_axis)
#     time_axis.sort()

#     data = {}
#     address_dic = {}
#     cnt = 0
#     for addr in address:
#         address_dic[addr] = 0
#     for device in devices:
#         data[device] = {}
#         for time in time_axis:
#             data[device][time] = 0
#     for row in rows:
#         if row.Message == "Device Info POST":
#             data[row.DeviceId][row.Date.strftime("%Y/%m/%d")] += 1
#             address_dic[row.Address] += row.PackQty
#     series = []
#     dic = {
#         "name": "",
#         "data": []
#     }

#     for name in data:
#         dic = {"name": name.split(" ")[0], "data": []}
#         for i in data[name]:
#             dic["data"].append(data[name][i]*2)
#         series.append(dic)

#     dic = {
#         "name": "",
#         "data": []
#     }
#     series2 = []
#     dic_1 = {}
#     s_1 = []
#     for address in address_dic:
#         addr = address.replace("-", "")
#         if addr[0] == "D":
#             s_1.append(addr[4:8])
#     s_1.sort()
#     for s in s_1:
#         dic_1[s] = []
#     for address in address_dic:
#         addr = address.replace("-", "")
#         if addr[0] == "D":
#             dic_1[addr[4:8]].append({"x": addr[:4], "y": address_dic[address]})

#     for name in dic_1:
#         dic = {"name": name, "data": []}
#         for i in dic_1[name]:
#             dic["data"].append(i)
#         series2.append(dic)

#     return series, time_axis, series2


# def send_data(rows):
#     time_axis = set()
#     devices = set()
#     address = set()
#     for row in rows:
#         if row.Message == "Device Info POST":
#             time_axis.add(row.Date.strftime("%Y/%m/%d"))
#             devices.add(row.DeviceId)
#             address.add(row.Address)
#     time_axis = list(time_axis)
#     time_axis.sort()

#     data = {}
#     address_dic = {}
#     cnt = 0
#     for addr in address:
#         address_dic[addr] = 0
#     for device in devices:
#         data[device] = {}
#         for time in time_axis:
#             data[device][time] = 0
#     for row in rows:
#         if row.Message == "Device Info POST":
#             data[row.DeviceId][row.Date.strftime("%Y/%m/%d")] += 1
#             address_dic[row.Address] += row.PackQty
#     series = []
#     dic = {
#         "name": "",
#         "data": []
#     }

#     for name in data:
#         dic = {"name": name.split(" ")[0], "data": []}
#         for i in data[name]:
#             dic["data"].append(data[name][i]*2)
#         series.append(dic)

#     return series, time_axis
