var username = "12"
var data_json

$(document).ready(function() {
    console.log("options");
    $.ajax({
      url: '/ajax.html',
      data: {
        'username': username
      },
      dataType: 'json',
      success: function (response) {
        $(document).ready(function() {
          var options = {
            chart: {
                height: 350,
                type: 'scatter',
                zoom: {
                    type: 'xy'
                }
            },
            series: [],
            dataLabels: {
                enabled: false
            },
            colors: ["#4099ff", "#2ed8b6", "#ff5370", "#ffb64d", "#00acc1","#ff7e53","#fff953","#53ff7b","#53fff9","#6153ff","#e253ff","#f06972"],
            grid: {
                xaxis: {
                    showLines: true
                },
                yaxis: {
                    showLines: true
                },
            },
            xaxis: {
                type: 'datetime',

            },
            yaxis: {
                max: 100
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return "%" +val 
                    }
                },
                x:{
                    formatter: function(val) {
                        
                        return (new Intl.DateTimeFormat('en-GB', { dateStyle: 'full', timeStyle: 'long' }).format(val)).split(' ')[5]
                    }
                }
            }
        }
          options["series"] = response["chart1"]
          var chart = new ApexCharts(
              document.querySelector("#scatter-chart-1"),
              options
          );
          chart.render();

          var options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            colors: ["#4099ff", "#2ed8b6", "#ff5370", "#ffb64d", "#00acc1","#ff7e53","#fff953","#53ff7b","#53fff9","#6153ff","#e253ff","#f06972"],
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [],
            xaxis: {
                categories: [],
            },
            yaxis: {
                title: {
                    text: ' '
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return  val + " scan"
                    }
                }
            }
        }
        options["series"]= response["chart2_series"]
        options["xaxis"]["categories"] = response["chart2_categories"]

        var chart = new ApexCharts(
            document.querySelector("#bar-chart-1"),
            options
        );
        chart.render();

        var options = {
            chart: {
                height: 5500,
                type: 'heatmap',
            },
            stroke: {
                width: 0
            },
            plotOptions: {
                heatmap: {
                    radius: 2,
                    enableShades: false,
                    colorScale: {
                        ranges: [{
                                from: 0,
                                to: 50,
                                color: '#ffb64d'
                            },
                            {
                                from: 51,
                                to: 100,
                                color: '#ff5370'
                            },
                        ],
                    },

                }
            },
            dataLabels: {
                enabled: true,
                style: {
                    colors: ['#fff']
                }
            },
            series: [],
            colors: ["#4099ff", "#00acc1", "#2ed8b6", "#ffb64d", "#ff5370"],
            xaxis: {
                type: 'category',
            },
            title: {
                text: 'Rounded (Range without Shades)'
            },
        }
        options["series"]= response["chart3_series"]
        var chart = new ApexCharts(
            document.querySelector("#heatmap-chart-2"),
            options
        );
        chart.render();
        var options = {
            chart: {
                height: 350,
                type: 'bar',
                stacked: true,
                stackType: '100%'
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                },

            },
            colors: ["#4099ff", "#00acc1", "#2ed8b6", "#ffb64d", "#ff5370"],
            stroke: {
                width: 1,
                colors: ['#fff']
            },
            series: [],

            title: {
                text: '100% Stacked Bar'
            },
            xaxis: {
                categories: [],
            },

            tooltip: {
                y: {
                    formatter: function(val) {
                        return val + "K"
                    }
                }
            },
            fill: {
                opacity: 1

            },

            legend: {
                position: 'top',
                horizontalAlign: 'left',
                offsetX: 40
            }
        }
        options["series"]= response["chart4_series"]
        options["xaxis"]["categories"]= response["chart4_categories"]
        var chart = new ApexCharts(
            document.querySelector("#bar-chart-4"),
            options
        );
        chart.render();
        
      });
      },
      error: function (response) {
        // alert the error if any error occured
        alert("response.chart.type");
    }
    });
});
